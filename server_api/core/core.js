var express = require('express');
var logger = require('morgan');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var myLogger = require('./middleware/logs');
var session = require('express-session'); // Сессии
var MongoStore = require('connect-mongo')(session); // Хранилище сессий в монгодб
var app = express();
// MiddleWare
app.middleware = function(){
    app.use(myLogger);
}
// app.modelsdb = function(){
//     var models = require('./models/index');
//     // console.log(models.User);
// }
// view engine setup
app.system_core = function(dirname){
    app.set('view engine', 'pug');
    app.set('views', path.join(dirname, 'views'));
    app.use(express.static(path.join(dirname, 'public')));
    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json({
        limit:"10kb"
    }));
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    // app.use(session({
    //     secret: 'Raker891@6532',
    //     // Замените на что нибудь
    //     resave: false,
    //     // Пересохранять даже если нету изменений
    //     saveUninitialized: true,
    //     // Сохранять пустые сессии
    //     store: new MongoStore({ mongooseConnection: require('mongoose').connection })
    //     // Использовать монго хранилище
    // }));
}
app.controllers = function(routs){
    app.use('/', routs['index']);
    app.use('/users', routs['users']);
    app.use('/api', routs['apis']);
    app.use('/api/crm', routs['api_crm']);
    app.use('/api/cms', routs['api_cms']);
    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    // error handler
    app.use(function(err, req, res, next) {
    // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });
}

module.exports = app;