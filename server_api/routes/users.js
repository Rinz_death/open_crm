var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
// Документы
router.get('/documents', function(req, res, next) {
  res.send('Шаблоны документов');
});
router.get('/documents:id', function(req, res, next) {
  res.send('Документ '+req.params.id);
});
router.get('/doc_create', function(req, res, next) {
  res.send('Создание документа');
});
// Тикеты/задачи
router.get('/tikets', function(req, res, next) {
  res.send('Задачи');
});
router.get('/tikets:id', function(req, res, next) {
  res.send('Задача '+req.params.id);
});
router.get('/tikets_create', function(req, res, next) {
  res.send('Создание задач');
});
// Записная книжка или комментарии
router.get('/note', function(req, res, next) {
  res.send('Записная книжка');
});
router.get('/note', function(req, res, next) {
  res.send('Записная книжка');
});
module.exports = router;
