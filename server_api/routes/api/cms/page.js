// Создание, редактирование страниц
var router = require('../index');
// Просмотр всех страниц
router.get('/page/:page', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Создание страницы
router.get('/page/create', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Обновляем страницу
router.get('/page/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});

// Отфильтровать по критериям
router.get('/page/filters/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
module.exports = router;
