// Управление пользователями, баны, регистрации
var router = require('../index');
// Все пользователи сайта
router.get('/users/page=:page', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Просмотр конкретного пользователя
router.get('/users/id=:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Ручное регистрирование пользователя
router.get('/users/create', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Пользователи в бане
router.get('/users/bans', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Удаленные пользователи за последние пол года
router.get('/users/delete', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
module.exports = router;
