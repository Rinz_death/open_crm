// Установка и настройка плагинов
var router = require('../index');
// Все плагины
router.get('/plugins/:page', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Установка плагинов
router.get('/plugins/install', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// фильтрация своих плагинов
router.get('/plugins/filters/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Поиск плагинов
router.get('/plugins/search/:search', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
module.exports = router;
