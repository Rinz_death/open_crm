// Логирование всех важных событии(ошибки, сбои, все исключения записываются в логи и просматриваются тут)
var router = require('../index');
//Все логи 
router.get('/logs/:page', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Почитать подробную информацию о конкретном логе
router.get('/logs/log/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Отфильтровать по критериям
router.get('/logs/filters/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
module.exports = router;
