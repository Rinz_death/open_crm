// Создание новых шаблонов документа, редактирование, удаление
var router = require('../index');
// шаблоны документов
router.get('/templates/page=:page', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Просмотр шаблона документа
router.get('/templates/id=:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
// Создание шаблона документа
router.get('/templates/create', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});
module.exports = router;
