var router = require('./index');

/* GET home page. */
router.get('/news', function(req, res, next) {
    //req.requestTime  <- middleware
    // console.log(req.models.userSchema);
    var news = {
        id: 1,
        date: "23.01.2018",
        time: "12.00",
        title: "News 1",
        min_text: "Какая то новость"
    }
    res.json(news);  
});

router.get('/news/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var news = {
        id: req.params.id, 
        title: "news 1", 
        text: "Эта такая то новость",
        date: "23.01.2017", 
        time: "12:00",
        user: "Этот человек виновен в сие новости",
        ratings: "88",
        comments: 
            {
                user: {
                    id:1,
                    message:"Полная лажа",
                    date: "23.01.2017",
                    time: "12:33",
                    req: 
                    {
                        user: {
                            id: 2, 
                            message: "Пшел вон",
                            date: "23.01.2017",
                            time: "12:44",
                        }
                    }
                }
            }
    }
  res.json(news);  
});

module.exports = router;
