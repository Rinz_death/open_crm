var router = require('./index');

/* GET home page. */
router.get('/users', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: [1,2,3,4,5]
    }
  res.json(user);  
});

router.get('/users/:id', function(req, res, next) {
    //req.requestTime  <- middleware
    var user = {
        id: req.params.id, 
        fio: "ФИО", 
        info: "Информация",
        message: {
            user:{
                id:2,
                text:"Tuta"
            }
        }, 
        news: [1,2,3,4],
        ratings: "88",
    }
  res.json(user);  
});

module.exports = router;
