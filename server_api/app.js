var favicon = require('serve-favicon');
var index = require('./routes/index');
var users = require('./routes/users');
var apis = require('./routes/api/controllers');
var api_crm = require('./routes/api/crm/controllers');
var api_cms = require('./routes/api/cms/controllers');
// var api_news = require('./routes/api/news');
// var api_users = require('./routes/api/users');
var app = require('./core/core');
app.middleware();
app.system_core(__dirname);
// app.modelsdb();
routs = {
  'index': index,
  'users': users,
  'apis': apis,
  'api_crm': api_crm,
  'api_cms': api_cms
  }
app.controllers(routs);

module.exports = app;
